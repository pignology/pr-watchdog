#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

//Log to omega kernel messages
void logmsg(char *msg) {
  FILE * pFile;
  pFile = fopen ("/dev/kmsg","w");
  fprintf (pFile, "pr-watchdog: %s\n", msg);
  fclose (pFile);
}

int main () {
  logmsg("pr-watchdog: start");
  FILE *fp;
  char psout[65535];
  char line[1024];

  while (1) {

    memset(psout, 0x00, 65535);
    memset(line, 0x00, 1024);

    fp = popen("/bin/ps -w", "r");
    if (fp == NULL) {
      printf("Failed to run command\n" );
      return 1;
    }

    while (fgets(line, sizeof(line)-1, fp) != NULL) {
      //printf("%s", line);
      strcat(psout, line);
    }

    pclose(fp);

    if (strstr(psout, "pigtail") == NULL) {
      logmsg("pigtail process not found, starting...");
      system("/etc/init.d/pigtail start");
    }

    if (strstr(psout, "pr-audio") == NULL) {
      logmsg("pr-audio process not found, starting...");
      system("/etc/init.d/pr-audio start");
    }

    sleep(5);
  }

  return 0;
}
